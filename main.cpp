//! [includes]

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "image_contrast.h"

using namespace std;
using namespace cv;
//! [includes]

string file_path;

ImageContrast image_contrast;

static void onMouse( int event, int x, int y, int, void* )
{
    //Pass event and position to curve controller
    image_contrast.mouseEventHandler(event, Point(x, y));
}

int main(int argc, char *argv[])
{
//    Checks whether user has started executable with an picture_image in the arguments
    if (argc > 1) file_path = argv[1];
    else
    {
        cout << "Selected picture_image file path: ";
        getline (cin, file_path);
    }

    cout << "\tImage contrast controller\n" << endl;
    cout << "\tAdjust nodes of value curve to define function applied on pixel values\n" << endl;
    cout << "\tControls:" << endl;
    cout << "\t - 'r', 'g' and 'b' changes current channel;" << endl;
    cout << "\t - 's' to save resulting image in current directory;" << endl;
    cout << "\t - Click \"Reset Contrast\" to position value curve nodes to initial position;" << endl;
    cout << "\t - Click \"Auto Contrast\" to use lowest and highest pixel values;" << endl;
    cout << "\t - 'Esc' to Quit." << endl;

//    file_path = R"(C:\Users\User\Pictures\still_life_less_contrast.jpg)";

    image_contrast = ImageContrast(file_path);
    setMouseCallback( "Value curve", onMouse ); //Add mouse events to window
    image_contrast.waitUserInput();

    return 0;
}
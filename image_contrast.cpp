//
// Created by User on 5/23/2021.
//

#include "image_contrast.h"

ImageContrast::ImageContrast(const string &file_path, const Point&  node_a, const Point&  node_b) {

    this->file_path = file_path;
    result_image = imread(file_path);

    //Set values for reset
    initial_nodes.node_a = node_a;
    initial_nodes.node_b = node_b;

    //Extremes
    findExtremes();

    curve_controller = CurveController();
    setNodePositions();
}

bool ImageContrast::selectClosestNode(Point mouse_position) {

    //Get distances from mouse mouse_position to nodes
    double distance_a = getMagnitude(node_a - mouse_position);
    double distance_b = getMagnitude(node_b - mouse_position);

    if(distance_a < distance_b && distance_a < node_size) {
        selected_node = &node_a;
        return true;
    }
    if(distance_b < distance_a && distance_b < node_size) {
        selected_node = &node_b;
        return true;
    }
    return false;
}

void ImageContrast::moveNode(Point mouse_position) {

    if(selected_node == &node_a && mouse_position.x >= node_b.x) return;
    else if(selected_node == &node_b && mouse_position.x <= node_a.x) return;
    else{
        selected_node->x = mouse_position.x;
        selected_node->y = mouse_position.y;

        NodePair* current_node_pair = &node_pairs[current_channel];

        if(selected_node == &node_a) {
            current_node_pair->node_a.x = mouse_position.x;
            current_node_pair->node_a.y = mouse_position.y;
        }
        else{
            current_node_pair->node_b.x = mouse_position.x;
            current_node_pair->node_b.y = mouse_position.y;
        }

        drawInputs();
    }
}

int ImageContrast::getValueAt(int x, int channel_enum) {
    NodePair current_pair = node_pairs[channel_enum];

    vector<double> node_x_positions{0, (float)current_pair.node_a.x, (float)current_pair.node_b.x, 255};
    vector<double> node_y_positions{0, (float)current_pair.node_a.y, (float)current_pair.node_b.y, 255};
    if(current_pair.node_a.x == x) return current_pair.node_a.y;
    if(current_pair.node_b.x == x) return current_pair.node_b.y;
    return (int)interpolate(node_x_positions, node_y_positions, x, true);
}


/*
 *  Returns interpolated value at x from parallel arrays ( xData, yData )
 *
 *  Assumes that xData has at least two elements, is sorted and is strictly monotonic increasing
 *  boolean argument extrapolate determines behaviour beyond ends of array (if needed)
 *  http://www.cplusplus.com/forum/general/216928/
 */
double ImageContrast::interpolate(vector<double> &xData, vector<double> &yData, double x, bool extrapolate) {
    int size = xData.size();

    int i = 0;                                                                  // find left end of interval for interpolation
    if ( x >= xData[size - 2] )                                                 // special case: beyond right end
    {
        i = size - 2;
    }
    else
    {
        while ( x > xData[i+1] ) i++;
    }
    double xL = xData[i], yL = yData[i], xR = xData[i+1], yR = yData[i+1];      // points on either side (unless beyond ends)
    if ( !extrapolate )                                                         // if beyond ends of array and not extrapolating
    {
        if ( x < xL ) yR = yL;
        if ( x > xR ) yL = yR;
    }

    double dydx = ( yR - yL ) / ( xR - xL );                                    // gradient

    return yL + dydx * ( x - xL );                                              // linear interpolation
}

void ImageContrast::changeChannel(int channel_enum) {

    channel_enum = clip(channel_enum, 0, 3);

    if(channel_enum == current_channel) return;

    current_channel = channel_enum;
    node_a = node_pairs[channel_enum].node_a;
    node_b = node_pairs[channel_enum].node_b;

    drawInputs();

}

int ImageContrast::clip(int value, int floor, int ceiling) {
    return max(floor, min(value, ceiling));
}

double ImageContrast::getMagnitude(Point point) {
    return sqrt(point.x * point.x + point.y * point.y);
}

void ImageContrast::findExtremes() {

    unsigned char* pixel_value; //Pointer to pixel at current iteration
    int* extremes = channel_extremes;
    pixel_value = result_image.ptr(0,0);

    extremes[0] = pixel_value[2];   //Red minimum
    extremes[1] = pixel_value[2];   //Red maximum
    extremes[2] = pixel_value[1];   //Green minimum
    extremes[3] = pixel_value[1];   //Green maximum
    extremes[4] = pixel_value[0];   //Blue minimum
    extremes[5] = pixel_value[0];   //Blue maximum

    bool all_maximum_contrast;  //Flag indicating if more parsing is necessary

    //Parse each pixel
    for (int x = 0; x < result_image.cols; ++x) {
        for (int y = 0; y < result_image.rows; ++y) {

            pixel_value = result_image.ptr(y, x);

                //Compare minimums and maximums and assign new values
                if(pixel_value[2] < extremes[0]) extremes[0] = pixel_value[2];
                if(pixel_value[2] > extremes[1]) extremes[1] = pixel_value[2];

                if(pixel_value[1] < extremes[2]) extremes[2] = pixel_value[1];
                if(pixel_value[1] > extremes[3]) extremes[3] = pixel_value[1];

                if(pixel_value[0] < extremes[4]) extremes[4] = pixel_value[0];
                if(pixel_value[0] > extremes[5]) extremes[5] = pixel_value[0];

            //Check if the pixels so far contained values 0 and 255 for each channel
            all_maximum_contrast = true;
            for (int i = 0; i < 3; ++i) {
                if(extremes[i * 2] != 255 || extremes[i * 2 + 1] != 0) all_maximum_contrast = false;
            }
            if(all_maximum_contrast) return;
        }
    }

}

void ImageContrast::moveNodesAuto() {

    NodePair* current_node_pair = &node_pairs[current_channel];

    current_node_pair->node_a.x = channel_extremes[current_channel * 2];
    current_node_pair->node_a.y = 0;
    current_node_pair->node_b.x = channel_extremes[current_channel * 2 + 1];
    current_node_pair->node_b.y = 255;

    node_a.x = channel_extremes[current_channel * 2];
    node_a.y = 0;
    node_b.x = channel_extremes[current_channel * 2 + 1];
    node_b.y = 255;

    drawResultImage();
    drawInputs();
}

void ImageContrast::setNodePositions() {
    //RGB channels
    for (int i = 0; i < 3; ++i) node_pairs[i] = initial_nodes;
    //Alpha channel
    node_pairs[3] = NodePair();

    current_channel = RED_CHANNEL;
    node_a = node_pairs[RED_CHANNEL].node_a;
    node_b = node_pairs[RED_CHANNEL].node_b;

    selected_node = nullptr;

    drawResultImage();
    drawInputs();
}

void ImageContrast::drawResultImage() {

    Mat new_image = imread(file_path);
    unsigned char * pixel;
    for (int x = 0; x < new_image.rows; ++x) {
        for (int y = 0; y < new_image.cols; ++y) {
            pixel = new_image.ptr(x, y);
            pixel[0] = getValueAt(pixel[0],BLUE_CHANNEL);
            pixel[1] = getValueAt(pixel[1],GREEN_CHANNEL);
            pixel[2] = getValueAt(pixel[2], RED_CHANNEL);
        }
    }
    result_image = new_image;

    imshow("Image contrast", result_image);
}

void ImageContrast::drawInputs() {

    Point curve_image_size = Point(curve_controller.auto_btn_rect.br().x + curve_controller.margin,
                                   curve_controller.auto_btn_rect.br().y + curve_controller.margin);

    Mat curve_image(curve_image_size.y, curve_image_size.x, CV_8UC3, Scalar(255, 255, 255));

    Scalar grey_50 = Scalar(127,127,127);
    Scalar grey_25 = Scalar(191,191,191);
    Scalar black = Scalar(0,0,0);

    //Draw label text
    const char *channel_names[4] = { "Red", "Green",
                              "Blue", "Alpha" };
    string button_text = "Channel: ";
    button_text += channel_names[current_channel];
    float font_scale = 1;

    Rect text_rect = curve_controller.label_rect;
    text_rect.x = text_rect.y = curve_controller.margin;
    text_rect.width -= curve_controller.margin * 2;
    text_rect.height -= curve_controller.margin * 2;

    Point text_position = Point(text_rect.tl().x + text_rect.width / 2, text_rect.tl().y + text_rect.height / 2);

    //Get computed text size to determine if it fits the button
    Size text_size = getTextSize(button_text,FONT_HERSHEY_SIMPLEX, font_scale, 1, nullptr);

    //If text is too high or too wide decrease the scale
    if(text_size.height > text_rect.height){
    font_scale = text_rect.height * font_scale / text_size.height;
    font_scale = round(font_scale * 10 - 0.5)/10;
    text_size = getTextSize(button_text,FONT_HERSHEY_SIMPLEX, font_scale, 1, nullptr);
    }
    else if(text_size.width > text_rect.width){
    font_scale = text_rect.width * font_scale / text_size.width;
    font_scale = round(font_scale * 10 - 0.5)/10;
    text_size = getTextSize(button_text,FONT_HERSHEY_SIMPLEX, font_scale, 1, nullptr);
    }

    //Get position of text (centered inside button) keeping text size in mind
    text_position.x = text_rect.x + text_rect.width / 2 - text_size.width / 2;
    text_position.y = text_rect.y + text_rect.height / 2 + text_size.height / 2;

    putText(curve_image, button_text, text_position, FONT_HERSHEY_SIMPLEX, font_scale, curve_controller.node_colors[current_channel], 1, LINE_AA);

    //Draw graph

    Rect graph = curve_controller.graph_rect;

    //Draw line on the black image, color given as scalar with BGR values

    //Major grid
    //Vertical line down the middle
    line(curve_image,
         Point((graph.x + graph.width / 2), graph.y),
         Point((graph.x + graph.width / 2), graph.y + graph.height),
         grey_50, 1, LINE_4);
    //Horizontal line down the middle
    line(curve_image,
         Point(graph.x, graph.y + graph.height / 2),
         Point(graph.x + graph.width, graph.y + graph.height / 2),
         grey_50, 1, LINE_4);
    //Minor grid
    //Vertical line left of middle
    line(curve_image,
         Point((graph.x + graph.width / 4), graph.y),
         Point((graph.x + graph.width / 4), graph.y + graph.height),
         grey_25, 1, LINE_4);
    //Vertical line right of middle
    line(curve_image,
         Point((graph.x + graph.width * 3 / 4), graph.y),
         Point((graph.x + graph.width * 3 / 4), graph.y + graph.height),
         grey_25, 1, LINE_4);
    //Horizontal line above middle
    line(curve_image,
         Point(graph.x, graph.y + graph.height / 4),
         Point(graph.x + graph.width, graph.y + graph.height / 4),
         grey_25, 1, LINE_4);
    //Horizontal line below middle
    line(curve_image,
         Point(graph.x, graph.y + graph.height * 3 / 4),
         Point(graph.x + graph.width, graph.y + graph.height * 3 / 4),
         grey_25, 1, LINE_4);

    //Set node color according to current channel
    Scalar node_color = curve_controller.node_colors[current_channel];

    //Node circles
    circle(curve_image,
           Point(graph.x + node_a.x, graph.y + 255 - node_a.y),
           node_size,node_color);
    circle(curve_image,
           Point(graph.x + node_b.x, graph.y + 255 - node_b.y),
           node_size,node_color);

    //Guide line
    line(curve_image,
         Point(graph.x, graph.y + graph.height),
         Point(graph.x + graph.width, graph.y), grey_25, 1, LINE_AA);

    //Lines connecting circles
    line(curve_image,
         Point(graph.x, graph.y + graph.height),
         Point(graph.x + node_a.x, graph.y + 255 - node_a.y),
         grey_50, 1, LINE_AA);
    line(curve_image,
         Point(graph.x + node_a.x, graph.y + 255 - node_a.y),
         Point(graph.x + node_b.x, graph.y + 255 - node_b.y),
         grey_50, 1, LINE_AA);
    line(curve_image,
         Point(graph.x + node_b.x, graph.y + 255 - node_b.y),
         Point(graph.x + graph.width, graph.y + 255 - graph.height),
         grey_50, 1, LINE_AA);

    //Draw buttons
    Mat button_image = imread(R"(resources\reset-contrast-button.jpg)");
    button_image.copyTo(curve_image(Rect(curve_controller.reset_btn_rect.tl().x, curve_controller.reset_btn_rect.tl().y, button_image.cols, button_image.rows)));
    button_image = imread(R"(resources\auto-contrast-button.jpg)");
    button_image.copyTo(curve_image(Rect(curve_controller.auto_btn_rect.tl().x, curve_controller.auto_btn_rect.tl().y, button_image.cols, button_image.rows)));

    imshow("Value curve", curve_image);
}

ImageContrast::ImageContrast() {

}

//Adjusts node positions using event info from main()
void ImageContrast::mouseEventHandler(int event, Point mouse_position) {

    if( event == EVENT_LBUTTONDOWN ) {

        //Start dragging a node if clicked inside the graph and close to a node
        if(curve_controller.graph_rect.contains(mouse_position)) {

            mouse_position -= curve_controller.graph_rect.tl(); //Mouse position includes the position of graph's rect
            mouse_position.y = 255 - mouse_position.y;  //For graph y0 is at the bottom
            //Mouse position ranges from 0-255 on both axis
            if(selectClosestNode(mouse_position))
                curve_controller.is_dragging = true;  //Start dragging
        }

        //Reset button clicked
        if(curve_controller.reset_btn_rect.contains(mouse_position)) setNodePositions();   //Reset to initial settings

        //Auto contrast button clicked
        if(curve_controller.auto_btn_rect.contains(mouse_position)) moveNodesAuto();    //Use calculated extremes
    }
    else if( event == EVENT_MOUSEMOVE && curve_controller.is_dragging && curve_controller.graph_rect.contains(mouse_position)){

        //User moves node around within the bounds
        mouse_position -= curve_controller.graph_rect.tl(); //Mouse position includes the position of graph's rect
        mouse_position.y = 255 - mouse_position.y;  //For graph y0 is at the bottom
        //Mouse position ranges from 0-255 on both axis
        moveNode(mouse_position);
    }
    else if(event == EVENT_LBUTTONUP || (event == EVENT_MOUSEMOVE && curve_controller.is_dragging && !curve_controller.graph_rect.contains(mouse_position))){

        //User released left mouse button or dragged the node out of bounds
        curve_controller.is_dragging = false;   //Stop dragging
        drawResultImage();
    }
}

void ImageContrast::waitUserInput() {

    string file_name;
    string extension;

    //Infinite cycle until user presses x key
    bool exit = false;
    while (!exit){
        int keystroke = waitKey(0); // Wait for a keystroke in the window
        switch (keystroke) {
            case 'r':
                changeChannel(RED_CHANNEL);
                break;
            case 'g':
                changeChannel(GREEN_CHANNEL);
                break;
            case 'b':
                changeChannel(BLUE_CHANNEL);
                break;
//            case 'a':
//                changeChannel(ALPHA_CHANNEL);
//                break;
            case 's':
                file_name = file_path.substr(file_path.find_last_of("/\\") + 1); //Get file name with extension
                extension = file_name.substr(file_name.find_last_of("/.") + 1);  //Get extension
                file_name = file_name.substr(0, file_name.size()-(extension.length() + 1));  //Get file name without extension
                imwrite(file_name + "_contrast_applied.jpg", result_image);
                break;
            case 27:    // Esc key pressed
                exit = true;
            default:
                break;
        }
    }
}

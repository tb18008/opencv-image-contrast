//
// Created by User on 5/23/2021.
//

#ifndef IMAGE_CONTRAST_IMAGE_CONTRAST_H
#define IMAGE_CONTRAST_IMAGE_CONTRAST_H

#include <iostream>
#include <utility>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

enum ChannelTypes {
    RED_CHANNEL  = 0,
    GREEN_CHANNEL  = 1,
    BLUE_CHANNEL  = 2,
    ALPHA_CHANNEL  = 3,
};

class ImageContrast {

    // Structures
    struct CurveController{
        int margin = 5;
        Rect label_rect = Rect(margin, margin, 256, 22);
        Rect graph_rect = Rect(label_rect.x, label_rect.y + label_rect.height + margin, 256, 256);
        Rect reset_btn_rect = Rect(graph_rect.x, graph_rect.y + graph_rect.height + margin, 124, 22);
        Rect auto_btn_rect = Rect(graph_rect.br().x - 124, graph_rect.br().y + margin, 124, 22);

        bool is_dragging = false;

        //Colors for ui node circle
        Scalar node_colors[4] = {
                Scalar(0,0,255),    //Blue
                Scalar(0,255,0),    //Green
                Scalar(255,0,0),    //Red
                Scalar(0,0,0),      //Black
        };
    };
    struct NodePair{
        Point node_a;
        Point node_b;
        NodePair(const Point& node_a = Point(63,63),const Point&  node_b = Point(191,191)){
            this->node_a = node_a;
            this->node_b = node_a;
        }
    };

    private:

        //Variables
        string file_path;   //Image file path to load image data from
        int channel_extremes[8];

        //Functions
        int clip(int value, int floor, int ceiling);
        void drawInputs();
        void findExtremes();
        double interpolate(vector<double> &xData, vector<double> &yData, double x, bool extrapolate);

        void changeChannel(int channel_enum);
        void drawResultImage();
        double getMagnitude(Point point);
        int getValueAt(int x, int channel_enum);
        bool selectClosestNode(Point mouse_position);
        void moveNode(Point mouse_position);
        void moveNodesAuto();
        void setNodePositions();

    public:

        //Variables

        Mat result_image;
        CurveController curve_controller;

        NodePair initial_nodes; //Position of nodes based on user input
        NodePair node_pairs[4]; //Saved node pair info for each channel

        //Short form nodes for current channel
        Point node_a;
        Point node_b;

        Point* selected_node;   //Pointer to node that is being moved around

        int current_channel = RED_CHANNEL;
        int node_size = 5;

        //Constructors

        explicit ImageContrast();
        explicit ImageContrast(const string &file_path, const Point& node_a = Point(63, 63), const Point& node_b = Point(191, 191));

        //Functions

        void mouseEventHandler(int event, Point mouse_position);
        void waitUserInput();
};


#endif //IMAGE_CONTRAST_IMAGE_CONTRAST_H
